#!/bin/bash
# Version 1.0

DIRECTORY="$(cd "$(dirname "$0")" && pwd)"
cd "$DIRECTORY" || exit 1

source ../../env.sh

# Registry is empty to use docker hub
REGISTRY_FROM=
IMAGE_FROM="$IMAGE_TRAEFIK"
TAG_FROM="$TAG_TRAEFIK"

REGISTRY_TO="local"
IMAGE_TO="$IMAGE_TRAEFIK"
TAG_TO="$TAG_TRAEFIK"

REGISTRY_LOGIN="user"
REGISTRY_PASSWORD="password"

source ../function.sh

buildImage